const {injectBabelPlugin} = require('react-app-rewired');
const path = require('path');
module.exports = function override(config, env) {
    // do stuff with the webpack config...
    config = injectBabelPlugin(['import', {libraryName: 'antd', libraryDirectory: 'es', style: 'css'}], config);
    // 配置路径别名
    config.resolve.alias['components'] = path.resolve(__dirname, 'src/components')
    config.resolve.alias['pages'] = path.resolve(__dirname, 'src/pages')
    config.resolve.alias['utils'] = path.resolve(__dirname, 'src/utils')
    return config;
};